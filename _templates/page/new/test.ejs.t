---
to: src/domain/<%= h.capitalize(name) %>/page/<%= h.capitalize(name) %>/<%= h.capitalize(name) %>.int.test.tsx
---
import { given<%= h.capitalize(name) %> } from './<%= h.capitalize(name) %>.test.helper';

describe('<%= h.capitalize(name) %> integration', () => {
    it('should ...', () => {
        given<%= h.capitalize(name) %>();
    });
});