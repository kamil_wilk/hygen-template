---
to: src/domain/<%= h.capitalize(name) %>/page/<%= h.capitalize(name) %>/<%= h.capitalize(name) %>.load.tsx
---
import { lazy } from 'react';
import * as React from 'react';
import { Loadable } from '../../../../app/components/Loadable/Loadable';

const Component = lazy(() => import('./<%= h.capitalize(name) %>'));

export const <%= h.capitalize(name) %>Loadable = () => (
  <Loadable>
    <Component />
  </Loadable>
);