---
to: src/domain/<%= h.capitalize(name) %>/page/<%= h.capitalize(name) %>/<%= h.capitalize(name) %>.tsx
---
import React from 'react';

const <%= h.capitalize(name) %> = () => {

    return (<div><%= h.capitalize(name) %></div>)
}

export default <%= h.capitalize(name) %>;