---
to: src/domain/<%= h.capitalize(name) %>/store/<%= h.capitalize(name) %>.selector.ts
---
import { createSelector } from 'reselect';

export namespace <%= h.capitalize(name) %>Selector {

}