---
to: src/domain/<%= h.capitalize(name) %>/store/<%= h.capitalize(name) %>.action.ts
---
export namespace <%= h.capitalize(name) %>Action {
    export namespace Types {
        export const TEST_ACTION = 'TEST_ACTION'
    }

    export interface TestAction {
        type: typeof TEST_ACTION,
        payload: unknown
    }

    export const testAction = (payload: unknown): TestAction => {
        type: Types.TEST_ACTION,
        payload
    }
}