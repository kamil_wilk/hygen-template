---
to: src/domain/<%= h.capitalize(name) %>/component/<%= h.capitalize(name) %>/<%= h.capitalize(name) %>.test.helper.tsx
---
import { screen, waitFor } from '@testing-library/react';
import { render } from '../../../../Shared/tests/test.helper';
import <%= h.capitalize(name) %> from './<%= h.capitalize(name) %>';

export const given<%= h.capitalize(name) %> = () => {
    render(<<%= h.capitalize(name) %> />);
};