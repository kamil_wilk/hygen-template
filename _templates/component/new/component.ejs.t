---
to: src/domain/<%= h.capitalize(name) %>/component/<%= h.capitalize(name) %>/<%= h.capitalize(name) %>.tsx
---
import React, {FC} from 'react';

export namespace <%= h.capitalize(name) %>Data {
    export interface Props {

    }
}

export const <%= h.capitalize(name) %>: FC<<%= h.capitalize(name) %>Data.Props> = ({}) => {

    return (<div><%= h.capitalize(name) %></div>)
}