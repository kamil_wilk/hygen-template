---
to: src/domain/<%= h.capitalize(name) %>/component/<%= h.capitalize(name) %>/<%= h.capitalize(name) %>.test.tsx
---
import { given<%= h.capitalize(name) %> } from './<%= h.capitalize(name) %>.test.helper';

describe('<%= h.capitalize(name) %> component', () => {
    it('should ...', () => {
        given<%= h.capitalize(name) %>();
    });
});