# Hygen templates for domain structure
## Install hygen
MacOS
```sh
$ brew tap jondot/tap
$ brew install hygen
```
NPM
```sh
$ npm i -g hygen
```

## Generate domain
```sh
$ hygen domain new --name myDomain
```

## Generate page (domain/page/)
```sh
$ hygen page new --name myPage --domain myDomain 
```

## Generate component (domain/component/)
```sh
$ hygen component new --name myComponent --domain myDomain 
```